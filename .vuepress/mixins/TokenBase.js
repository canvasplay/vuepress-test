import tokens from './../../dist/web/tokens.json'
import properties from './../../dist/web/properties.json'
import flatten from './../utils/flatten'
export default {
  props: {
    token: {
      type: String,
      default: '',
      optional: true
    }
  },
  computed: {
    value(){
      const path = this.token.split('.');
      var t = tokens;
      while(t && path[0]){
        t=t[path.shift()];
      }
      return t;
    },
    property(){
      const path = this.token.split('.');
      var p = properties;
      while(p && path[0]){
        p=p[path.shift()];
      }
      if(!p.attributes) return flatten(p);
      else return p;
    }
  }
}