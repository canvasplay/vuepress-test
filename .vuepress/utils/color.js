
let hexToRgb = function(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });
    
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};

let contrast = function(color){
  
  var rgb;
  
  //convert to rgb if hex
  if(color.indexOf('#')===0){
    rgb = this.hexToRgb(color);
    rgb = 'rgb('+rgb.r+','+rgb.g+','+rgb.b+')';
  }
  else{
    rgb = color;
  }

    // Strip everything except the integers eg. "rgb(" and ")" and " "
    rgb = rgb.split(/\(([^)]+)\)/)[1].replace(/ /g, '');

    // map RGB values to variables
    var r = parseInt(rgb.split(',')[0], 10),
        g = parseInt(rgb.split(',')[1], 10),
        b = parseInt(rgb.split(',')[2], 10);

    // calculate contrast of color (standard grayscale algorithmic formula)
    var contrast = (Math.round(r * 299) + Math.round(g * 587) + Math.round(b * 114)) / 1000;

    return (contrast >= 128) ? '#000000' : '#FFFFFF';
};

export default {
  hexToRgb,
  contrast
};