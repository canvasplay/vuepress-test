module.exports = {
  title: 'Awesome Docs',
  description: 'This is also an awesome tagline for sure!',
  themeConfig: {
    searchPlaceholder: 'Search',
    nav: [
      { text: 'Home', link: '/' },
      {
        text: 'Examples',
        items: [
          { text: 'Token Sampler', link: '/demo/tokens' },
          { text: 'Markdown Examples', link: '/demo/test' }
        ]
      },
      { text: 'Test', link: '/demo/test' },
      { text: 'VuePress', link: 'https://vuepress.vuejs.org/' }
    ],
    //sidebar: 'auto',
    sidebar: [
      {
        title: 'Demos',
        path: '/demo/',
        collapsable: false,
        children: [
          '/demo/examples',
          '/demo/test',
          '/demo/settings'
        ]
      },
      {
        title: 'Visual Tokens',
        path: '/demo/visual-tokens/',
        collapsable: false,
        children: [
          '/demo/visual-tokens/color',
          '/demo/visual-tokens/text',
          '/demo/visual-tokens/space',
          '/demo/visual-tokens/data',
          '/demo/visual-tokens/misc'
        ]
      },
      '/docs/',
    ],
    sidebarDepth: 2,
    lastUpdated: 'Last Updated',
  },
  plugins: {

  },
  head: [
    //['link', { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.1.4/tailwind.min.css' }],
    ['link', { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/tailwindcss/dist/utilities.min.css' }],
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Mono:400,400i,700,700i|Roboto+Slab:400,700,900|Roboto:400,400i,700,700i,900,900i&display=swap' }],
  ]/*,
  postcss: {
    plugins: [
      require("tailwindcss"),//("docs/.vuepress/tailwind.config.js")
      require("autoprefixer"),
    ]
  }*/,
  module: {
    rules: [
      // ... other rules omitted

      // this will apply to both plain `.scss` files
      // AND `<style lang="scss">` blocks in `.vue` files
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  }
}
