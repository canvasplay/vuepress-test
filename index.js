const StyleDictionary = require('style-dictionary').extend({
  source: [
    'properties/**/*.json'
  ],
  platforms: {
    web: {
      transformGroup: 'custom/web',
      buildPath: 'dist/web/',
      prefix: 'token',
      files: [
        {
          destination: 'tokens.scss',
          format: 'scss/map-deep'
        },
        {
          destination: 'tokens.json',
          format: 'json/nested',
        },
        {
          destination: 'properties.json',
          format: 'json',
        }
      ]
    }
  }
});

StyleDictionary.registerTransform({
  name: 'font/size/rem',
  type: 'value',
  matcher: function (prop) {
    return prop.attributes.category === 'font' && prop.attributes.type === 'size';
  },
  transformer: function (prop) {
    return `${prop.value}rem`;
  }
});

StyleDictionary.registerTransform({
  name: 'space/rem',
  type: 'value',
  matcher: function (prop) {
    return prop.attributes.category === 'space';
  },
  transformer: function (prop) {
    return prop.value ? `${prop.value}rem` : '0';
  }
});

StyleDictionary.registerTransform({
  name: 'border/radius/rem',
  type: 'value',
  matcher: function (prop) {
    return prop.attributes.category === 'border' && prop.attributes.type === 'radius';
  },
  transformer: function (prop) {
    return prop.value ? `${prop.value}rem` : '0';
  }
});

StyleDictionary.registerTransform({
  name: 'breakpoint/px',
  type: 'value',
  matcher: function (prop) {
    return prop.attributes.category === 'breakpoint';
  },
  transformer: function (prop) {
    return prop.value ? `${prop.value}px` : '0';
  }
});

StyleDictionary.registerTransform({
  name: 'asset/font',
  type: 'value',
  matcher: function (prop) {
    return (
      prop.attributes.category === 'asset' &&
      prop.attributes.type === 'font' &&
      ['name', 'otf', 'woff', 'woff2', 'ttf', 'svg'].indexOf(prop.attributes.subitem) !== -1
    );
  },
  transformer: function (prop) {
    return `"${prop.value}"`;
  }
});

StyleDictionary.registerTransformGroup({
  name: 'custom/web',
  transforms: [
    'attribute/cti',
    'name/cti/kebab',
    'time/seconds',
    'content/icon',
    'size/rem',
    'font/size/rem',
    'space/rem',
    'border/radius/rem',
    'breakpoint/px',
    'color/hex',
    'asset/font'
  ]
});

StyleDictionary.buildAllPlatforms();