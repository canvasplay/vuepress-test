# This is a test page

[[toc]]


<pre class="text-gray-300">
.
├── demo
│   ├── examples.md
│   ├── img
│   │   ├── awesome.jpg
│   │   ├── ex1.png
│   │   └── ex2.png
│   ├── README.md
│   ├── settings.md
│   ├── test.md
│   └── tokens.md
├── docs
│   ├── <span class="bg-green-400">components</span>
│   │   └── README.md
│   ├── configuration
│   │   └── README.md
│   ├── getting-started
│   │   └── README.md
│   └── README.md
├── package.json
├── package-lock.json
└── README.md
</pre>

Sed malesuada tincidunt lorem, vel congue lectus condimentum eu. Vestibulum at mi vitae ligula porttitor scelerisque. Proin a finibus risus. Nunc non augue est. Duis non risus urna. Vivamus nibh mauris, sodales et nulla et, faucibus interdum tortor. Sed rhoncus nibh nec pulvinar fermentum. Integer mattis vulputate arcu, id molestie ex semper ut. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas ultricies nunc velit, eget imperdiet nisi molestie in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet ipsum tempor, ultrices leo nec, facilisis mauris. Phasellus congue et ligula eget efficitur. Nam faucibus libero quam, ut vulputate nisl scelerisque et. Nullam pretium dapibus dolor, nec rutrum sem ullamcorper at. In metus purus, ultricies ut massa sit amet, congue interdum justo.

## Zoomable images

Just by adding the `#zoomable` hash at the end of your images urls you can activate an awesome zoomable image just like in medium.com.

![relative path image](./img/awesome.jpg#zoomable)

Sed malesuada tincidunt lorem, vel congue lectus condimentum eu. Vestibulum at mi vitae ligula porttitor scelerisque. Proin a finibus risus. Nunc non augue est. Duis non risus urna. Vivamus nibh mauris, sodales et nulla et, faucibus interdum tortor. Sed rhoncus nibh nec pulvinar fermentum. Integer mattis vulputate arcu, id molestie ex semper ut. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas ultricies nunc velit, eget imperdiet nisi molestie in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet ipsum tempor, ultrices leo nec, facilisis mauris. Phasellus congue et ligula eget efficitur. Nam faucibus libero quam, ut vulputate nisl scelerisque et. Nullam pretium dapibus dolor, nec rutrum sem ullamcorper at. In metus purus, ultricies ut massa sit amet, congue interdum justo.

## Using Badges <Badge text="beta" type="warning"/> <Badge text="default theme"/>

Sed malesuada tincidunt lorem, vel congue lectus condimentum eu. Vestibulum at mi vitae ligula porttitor scelerisque. Proin a finibus risus. Nunc non augue est. Duis non risus urna. Vivamus nibh mauris, sodales et nulla et, faucibus interdum tortor. Sed rhoncus nibh nec pulvinar fermentum. Integer mattis vulputate arcu, id molestie ex semper ut. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas ultricies nunc velit, eget imperdiet nisi molestie in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet ipsum tempor, ultrices leo nec, facilisis mauris. Phasellus congue et ligula eget efficitur. Nam faucibus libero quam, ut vulputate nisl scelerisque et. Nullam pretium dapibus dolor, nec rutrum sem ullamcorper at. In metus purus, ultricies ut massa sit amet, congue interdum justo.

## How to make a table?

This paragraph is just here to show you a sample on how you can write content in markdown formatted as nice table.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |


## Download files

Just by adding the `#zoomable` hash at the end of your images urls you can activate an awesome zoomable image just like in medium.com.

[download this](./img/awesome.jpg)
<a href="./img/awesome.jpg" download="download.jpg">download this html</a>


## Blockquote

As Kanye West said:

> We're living the future so
> the present is our past.


::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger SUPER DANGEROUS TIP
This is a dangerous warning
:::

::: danger STOP
Danger zone, do not proceed
:::

::: details Click me to view the code
```js
console.log('Hello, VuePress!')
```
:::


## TaskList example

- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
