# Visual Tokens

A collection of simple Vue components created to visually display the value of different design token categories.
Here you can find all exsisting components and some examples.