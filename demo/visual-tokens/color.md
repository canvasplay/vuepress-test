# Color

## ColorTint

As seen on https://biings.design/#/color also similar to https://codyhouse.co/ds/globals/colors.

<ExampleBox>
  <ColorTintList token="color.base.blue" label="Blue" class="w-full text-center" />
</ExampleBox>

```md
  <ColorTintList token="color.base.blue" label="Blue" />
```
<ExampleBox>
  <ColorTint token="color.base.purple.700" label="Purple" class="w-full mb-5" />
  <ColorTint token="color.base.purple.900" label="Purple Dark" class="w-full" />
</ExampleBox>

```md
  <ColorSample token="color.base.purple.700" label="Purple" />
  <ColorSample token="color.base.purple.900" label="Purple Dark" />
```

## ColorSample

Use it to display a big color samples by referencing a token key name.

<div class="flex flex-wrap my-8">
  <ColorSample token="color.base.purple.700" label="Purple" class="w-full" />
  <ColorSample token="color.base.purple.900" label="Purple Dark" class="w-full" />
</div>

```md
  <ColorSample token="color.base.purple.700" label="Purple" />
  <ColorSample token="color.base.purple.900" label="Purple Dark" />
```

## ColorSampleList

<ColorSampleList token="color.base.grey" />

```md
  <ColorSampleList token="color.base.grey" />
```


## ColorSwatch

Use it to display a big color samples by referencing a token key name.

<div class="flex flex-wrap my-8">
  <ColorSwatch token="color.base.blue.700" name="Blue" label="1" class="w-full" />
  <ColorSwatch token="color.base.blue.200" name="Blue Light" label="2" class="w-full md:w-1/2" />
  <ColorSwatch token="color.base.blue.900" name="Blue Dark" label="3" class="w-full md:w-1/2" />
</div>

```md
  <ColorSwatch token="color.base.blue.700" label="Blue" class="w-full" />
  <ColorSwatch token="color.base.blue.200" label="Blue Light" class="w-1/2" />
  <ColorSwatch token="color.base.blue.900" label="Blue Dark" class="w-1/2" />
```


## ColorShade

Use it to display a sample text with a font size equivalent to the given token value.

<div class="flex flex-wrap my-8">
  <Shade token="color.base.grey.100" label="100" />
  <Shade token="color.base.grey.200" label="200" />
  <Shade token="color.base.grey.300" label="300" />
  <Shade token="color.base.grey.400" label="400" />
  <Shade token="color.base.grey.500" label="500" :base="true" />
  <Shade token="color.base.grey.600" label="600" />
  <Shade token="color.base.grey.700" label="700" />
  <Shade token="color.base.grey.800" label="800" />
  <Shade token="color.base.grey.900" label="900" />
</div>

```md
  <Shade token="color.base.grey.100" label="100" />
  <Shade token="color.base.grey.200" label="200" />
  <Shade token="color.base.grey.300" label="300" />
  <Shade token="color.base.grey.400" label="400" />
  <Shade token="color.base.grey.500" label="500" base="true" />
  <Shade token="color.base.grey.600" label="600" />
  <Shade token="color.base.grey.700" label="700" />
  <Shade token="color.base.grey.800" label="800" />
  <Shade token="color.base.grey.900" label="900" />

```