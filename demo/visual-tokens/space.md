# Space

## SpaceBox

<div class="flex my-8 text-blue-800">
  <SpaceBox class="m-1" token="space.none" label="none" />
  <SpaceBox class="m-1" token="space.xs" label="xs" />
  <SpaceBox class="m-1" token="space.sm" label="sm" />
  <SpaceBox class="m-1" token="space.md" label="md" />
  <SpaceBox class="m-1" token="space.lg" label="lg" />
  <SpaceBox class="m-1" token="space.xl" label="xl" />
</div>

## SpaceInline

<div class="flex my-8 text-blue-800">
  <SpaceInline class="m-1" token="space.none" label="none" />
  <SpaceInline class="m-1" token="space.xs" label="xs" />
  <SpaceInline class="m-1" token="space.sm" label="sm" />
  <SpaceInline class="m-1" token="space.md" label="md" />
  <SpaceInline class="m-1" token="space.lg" label="lg" />
  <SpaceInline class="m-1" token="space.xl" label="xl" />
</div>

## SpaceStack

<div class="flex flex-col my-8 text-blue-800">
  <SpaceStack class="m-1" token="space.none" label="none" />
  <SpaceStack class="m-1" token="space.xs" label="xs" />
  <SpaceStack class="m-1" token="space.sm" label="sm" />
  <SpaceStack class="m-1" token="space.md" label="md" />
  <SpaceStack class="m-1" token="space.lg" label="lg" />
  <SpaceStack class="m-1" token="space.xl" label="xl" />
</div>