# Text

A collection of simple Vue components created to visually display the value of different design token categories.
Here you can find all exsisting components and some examples.


### FontStyle

<!--<ExampleBox class="overflow-hidden px-0 pb-12">
  <FontStyleList token="font.style" label="Sample Text" />
</ExampleBox>-->

<ExampleBox class="overflow-hidden px-0 pb-12">
  <FontStyle token="font.style.h1" label="H1 Headings" />
  <FontStyle token="font.style.h2" label="H2 Headline" />
  <FontStyle token="font.style.h3" label="H3 Headline" />
  <FontStyle token="font.style.h4" label="H4 Headline" />
  <FontStyle token="font.style.h5" label="H5 Headline" />
  <FontStyle token="font.style.h6" label="H6 Headline" />
  <FontStyle token="font.style.button" />
  <FontStyle token="font.style.caption" />
  <FontStyle token="font.style.overline" />
</ExampleBox>


### FontSize

Use it to display a sample text with a font size equivalent to the given token value.

<div class="my-8">
  <FontSize class="p-4" token="font.size.micro" label="Micro" />
  <FontSize class="p-4" token="font.size.mini" label="Mini" />
  <FontSize class="p-4" token="font.size.small" label="Small" />
  <FontSize class="p-4" token="font.size.medium" label="Medium" />
  <FontSize class="p-4" token="font.size.extra" label="Extra" />
  <FontSize class="p-4" token="font.size.h6" label="Heading 6" />
  <FontSize class="p-4" token="font.size.h5" label="Heading 5" />
  <FontSize class="p-4" token="font.size.h4" label="Heading 4" />
  <FontSize class="p-4" token="font.size.h3" label="Heading 3" />
  <FontSize class="p-4" token="font.size.h2" label="Heading 2" />
  <FontSize class="p-4" token="font.size.h1" label="Heading 1" />
  <FontSize class="p-4" token="font.size.display-1" label="Display 1" />
  <FontSize class="p-4" token="font.size.display-2" label="Display 2" />
</div>

### FontWeight

Use it to display a sample text with a font weight equivalent to the given token value.

<div class="my-8">
<ExampleBox>
  <FontWeight class="p-4" token="font.weight.normal" label="Normal" />
  <FontWeight class="p-4" token="font.weight.bold" label="Bold" />
</ExampleBox>
</div>

```md
  <FontWeight class="p-4" token="font.weight.normal" label="Normal" />
  <FontWeight class="p-4" token="font.weight.bold" label="Bold" />
```

### FontLeading

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum. Cras gravida, velit et iaculis blandit, lacus augue eleifend magna, vel auctor nulla odio sit amet justo. Praesent a ornare augue, id vestibulum quam. Mauris quis sapien elit. Nunc vitae lectus varius, finibus nisi eget, bibendum nulla. Donec ut sollicitudin nisi, ac mattis urna.


<div class="my-8">
  <FontLeading class="p-4" token="font.leading.none">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  <FontLeading class="p-4" token="font.leading.compact">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  <FontLeading class="p-4" token="font.leading.medium">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  <FontLeading class="p-4" token="font.leading.normal">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  <FontLeading class="p-4" token="font.leading.loose">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  <FontLeading class="p-4" token="font.leading.extra">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium neque varius enim fringilla, ut egestas massa hendrerit. Donec dui turpis, fringilla eget tellus luctus, maximus ornare justo. Duis non mollis lacus, vel faucibus ipsum.
  </FontLeading>
  
</div>