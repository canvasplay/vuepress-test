# Data

A collection of simple Vue components created to visually display the value of different design token categories.
Here you can find all exsisting components and some examples.

## TokensTable

Shows a name/value table listing all descendant tokens for the given token name.
Use it with ptoperty token groups. Do not use it with final property tokens or will just show an empty table.

<TokensTable token="font.weight" prefix="$token-" />
<TokensTable token="font.weight.normal" />

```md
  <TokensTable token="font.weight" prefix="$token-" />
  <TokensTable token="font.weight.normal" />
```

## TokenObject

Displays code block containing the JSON represtation for the value of the given token.

<TokenObject token="font.weight.bold" class="text-white" />

<TokenObject token="font.family" class="text-white" />
```md
  <TokenObject token="font.weight.bold"/>
  <TokenObject token="font-family" />

```

## TokenProperty

Displays code block containing the JSON represtation of the flatten property object for the given token.

<TokenProperty token="font.weight.bold" class="text-white" />
<TokenProperty token="font.family" class="text-white" />

```md
  <TokenProperty token="font.weight.bold" class="text-white" />
  <TokenProperty token="font.family" class="text-white" />

```