# Misc

A collection of simple Vue components created to visually display the value of different design token categories.
Here you can find all exsisting components and some examples.

## Shadows

### BoxShadow

<TokensTable token="shadow" />

<div class="my-8">
<ExampleBox class="w-full flex">
  <BoxShadow class="m-1" token="shadow.none" label="none" />
  <BoxShadow class="m-1" token="shadow.sm" label="sm" />
  <BoxShadow class="m-1" token="shadow.md" label="md" />
  <BoxShadow class="m-1" token="shadow.lg" label="lg" />
  <BoxShadow class="m-1" token="shadow.xl" label="xl" />
  <BoxShadow class="m-1" token="shadow.2xl" label="2xl" />
</ExampleBox>
</div>


## BorderRadius

### BorderRadius

Display rounded boxes using the given token value

<div class="flex flex-wrap">
  <BorderRadius class="m-1" token="border.radius.none" label="none" />
  <BorderRadius class="m-1" token="border.radius.md" label="md" />
  <BorderRadius class="m-1" token="border.radius.lg" label="lg" />
  <BorderRadius class="m-1" token="border.radius.xl" label="xl" />
  <BorderRadius class="m-1" token="border.radius.2xl" label="2xl" />
</div>

```md
<div class="flex flex-wrap">
  <BorderRadius class="m-1" token="border.radius.none" label="none" />
  <BorderRadius class="m-1" token="border.radius.md" label="md" />
  <BorderRadius class="m-1" token="border.radius.lg" label="lg" />
  <BorderRadius class="m-1" token="border.radius.xl" label="xl" />
  <BorderRadius class="m-1" token="border.radius.2xl" label="2xl" />
</div>

```


### Components

<div class="overflow-hidden border border-solid border-gray-400 p-4 bg-gray-200 flex justify-around items-center text-sm py-8 rounded">
<div class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer">
  Button
</div>
</div>